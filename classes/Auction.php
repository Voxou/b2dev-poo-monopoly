<?php


class Auction extends Game
{
    public $playersInAuction;
    public $placeInAuction;
    public $currentWinningAuctionValue;
    public $currentWinningAuctionPlayer;

    public function __construct($place)
    {
        $this->playersInAuction = Game::$players;
        $this->playersInAuction = $place;
        parent::__construct();
    }

    public function removePlayerFromAuction()
    {

    }

    public function newCurrentWinningAuction($value, $player)
    {
        $this->currentWinningAuctionValue = $value;
        $this->currentWinningAuctionPlayer = $player;
    }


}