<?php
include_once('Game.php');


class Board extends Game
{
    public $currency;
    public $startingMoneyValue;
    public $goTileMoneyValue;
    public $numberOfHouses;
    public $numberOfHotels;
    public $numberOfTiles;
    public $numberOfPlayers;
    public $activePlayer;
    public $nextActivePlayer;

    public function __construct($rules, $tiles)
    {
        $this->hydrateBoard($rules, $tiles);
        parent::__construct();
    }

    public function hydrateBoard($rules, $tiles)
    {
        $this->currency = $rules['currency'];
        $this->startingMoneyValue = $rules['startingMoneyValue'];
        $this->goTileMoneyValue = $rules['goTileMoneyValue'];
        $this->numberOfHouses = $rules['numberOfHouses'];
        $this->numberOfHotels = $rules['numberOfHotels'];
        $this->numberOfTiles = count($tiles);
    }
}