<?php


class Dice
{
    public $defaultMaxValue = 6;
    public $value;

    //lancer un ou plusieurs dés
    public function rollDices($numberOfDices, $maxValue)
    {
        $result = [];
        for($i=1; $i<=$numberOfDices; $i++)
        {
            if($maxValue == null)
            {
                $result[] = rand(1, $this->defaultMaxValue);
            } else {
                $result[] = rand(1, $maxValue);
            }
        }
        return $result;
    }

    //renvoyer le résultat d'un lancé de dés sous la forme d'un tableau
    public function getResultOfDicesRoll($numberOfDices, $maxValue)
    {
        $result = $this->rollDices($numberOfDices, $maxValue);
        $sum = array_sum($result);
        $double = false;
        if(count($result) == 2)
        {
            if($result[0] == $result[1])
            {
                $double = true;
            }
        }
        return [
            'result' => $result,
            'sum' => $sum,
            'double' => $double,
        ];
    }
}