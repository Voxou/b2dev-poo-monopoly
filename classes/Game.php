<?php


class Game
{
    public static $board;
    public static $players = [];
    public static $auctionInProgress = false;

    public function __construct()
    {
        switch (get_class($this)){
            case Board::class:
                self::$board = $this;
                break;
            case Player::class:
                self::$players[] = $this;
                break;
            case Auction::class:
                self::$auctionInProgress = true;
                break;
        }
    }
}