<?php
include_once('Tile.php');
include_once('Player.php');


class Place extends Tile
{
    public $subtype;
    public $color;
    public $price;
    public $owner = null;
    public $houses = null;
    public $hotel = null;

    public function __construct($datas)
    {
        $this->hydratePlace($datas);
        parent::__construct($datas);
    }

    public function hydratePlace($datas)
    {
        $this->color = $datas['color'];
        $this->price = $datas['price'];
        $this->subtype = $datas['subtype'];
    }

    //peut être achetée
    public function boughtBy(Player $owner)
    {
        $this->owner = $owner;
    }
}