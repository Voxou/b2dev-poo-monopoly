<?php
include_once ('Game.php');
include_once('Dice.php');
include_once('Place.php');
include_once ('Tax.php');


class Player extends Game
{
    public $name;
    public $money;
    public $position = 1;
    public $properties = [];
    public $cards = [];
    public $isInJail = false;
    public $dice;

    public function __construct($name)
    {
        $this->name = $name;
        $this->money = Game::$board->startingMoneyValue;
        $this->dice = new Dice();
        parent::__construct();
    }

    //récupérer le string du capital et de la monnaie pour l'affichage
    public function getMoneyWithCurrency()
    {
        return $this->money.' '.Game::$board->currency;
    }

    //lancer un ou plusieurs dés
    public function rollDices($numberOfDices = 2, $maxValue = null)
    {
        return $this->dice->getResultOfDicesRoll($numberOfDices, $maxValue);
    }

    //se déplacer d'un certain nombre de cases
    public function move($numberOfMovements)
    {
        $this->position += $numberOfMovements;
        //cas d'un tour complet de plateau
        if($this->position > Game::$board->numberOfTiles)
        {
            $this->position -= Game::$board->numberOfTiles;
            //argent reçu par le passage en case départ
            $this->money += Game::$board->goTileMoneyValue;
        }
    }

    //acheter une propriété
    public function buyPlace(Place $place)
    {
        $this->properties[] = $place;
        $place->boughtBy($this);
        $this->money -= $place->price;
    }

    //garder une carte
    public function keepCard(Card $card)
    {
        $this->cards[] = $card;
    }

    //TODO : ne pas mettre la position en dur
    //aller en prison
    public function getInJail()
    {
        $this->position = 11;
        $this->isInJail = true;
    }

    //sortir de prison
    public function getOutOfJail()
    {
        $this->isInJail= false;
    }

    //payer une tax
    public function payTax(Tax $tax)
    {
        $this->money -= $tax->taxValue;
    }
}