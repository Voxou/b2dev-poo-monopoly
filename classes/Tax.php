<?php
include_once('Tile.php');


class Tax extends Tile
{
    public $taxValue;

    public function __construct($datas)
    {
        $this->hydrateTax($datas);
        parent::__construct($datas);
    }

    public function hydrateTax($datas)
    {
        $this->taxValue = $datas['taxValue'];
    }
}