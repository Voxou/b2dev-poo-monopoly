<?php


class Tile
{
    public $position;
    public $entitle;
    public $type;

    public function __construct($datas)
    {
        $this->hydrateTile($datas);
    }

    public function hydrateTile($datas)
    {
        $this->position = $datas['position'];
        $this->entitle = $datas['entitle'];
        $this->type = $datas['type'];
    }
}