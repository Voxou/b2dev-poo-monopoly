<?php
include_once('data/frenchClassicEuro.php');
include_once('classes/Board.php');
include_once('classes/Player.php');

echo('test création du plateau');
$board = new Board($rules, $tiles);
var_dump($board);
var_dump(Game::$board);

echo('test création de plusieurs joueurs');
$player = new Player('Goku');
$player2 = new Player('Vegeta');
var_dump($player);
var_dump($player2);
var_dump(Game::$players);

echo('test affichage argent');
var_dump($player->getMoneyWithCurrency());

echo('test de lancé de dé');
$roll = $player->rollDices();
var_dump($roll);

echo('test de déplacement');
$player->move($roll['sum']);
var_dump($player->position);

echo('test de téléportation');
$player->position = 39;
var_dump($player->position);

echo('test du tour de plateau');
$roll = $player->rollDices();
var_dump($roll['sum']);
$player->move($roll['sum']);
var_dump($player->position);

echo('test de l\'argent reçu en passant par la case départ');
var_dump($player->getMoneyWithCurrency());

echo('test création d\'une tuile');
$tile = new Tile(($tiles['tile1']));
var_dump($tile);

echo('test création d\'une place');
$place = new Place($tiles['tile2']);
var_dump($place);

echo('test création d\'une taxe');
$tax = new Tax($tiles['tile5']);
var_dump($tax);

echo('test d\'achat d\'une propriété');
$player->buyPlace($place);
var_dump($player->properties);
var_dump($place->owner);
var_dump($player->getMoneyWithCurrency());

echo('test de paiement de taxe');
$player->payTax($tax);
var_dump($player->getMoneyWithCurrency());